#!/usr/bin/env bash


autoreconf -i || exit 1
./configure || exit 1
make || exit 1
./testing
gcovr -r . || exit 1
gcovr -r . --html --html-details -o coverage/coverage.html || exit 1
gcovr -r . --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml
