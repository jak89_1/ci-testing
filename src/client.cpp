#include "client.h"

/** 
 * @file       client.cpp
 * @brief      Just a simple client source file
 * @details    that does nothing special, like all other files do
 * @author     jak1
 * @copyright  MIT License, see repository LICENSE file
 */

Client::Client() { }

Client::~Client() { }


void Client::init()
{
    setRunning(true);
}

int Client::loop()
{
    int x = 0;
    while(isRunning())
    {
        x++;
        test(x);
        //testing...
        if (x >= 5)
            break;
    }
    return 0;
}

void Client::test(int x)
{
    std::cout << "testing: " << x << std::endl;
}
