#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>


/** 
 * @file       client.h
 * @brief      Just a simple client header file
 * @details    that does nothing special
 * @author     jak1
 * @copyright  MIT License, see repository LICENSE file
 */
class Client
{
public:
    Client();
    ~Client();

    void init();
    int loop();
    bool isRunning()
        { return running; }
    void setRunning(bool run)
        { running = run; }

private:
    
    void test(int x);
    bool running = false;
};

#endif // CLIENT_H
