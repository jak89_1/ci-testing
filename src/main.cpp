
#include "debug.h"

#include "client.h"

/** 
 * @file       main.cpp
 * @brief      Just a simple main source file
 * @details    that does nothing special
 * just multiline description testing
 * @author     jak1
 * @copyright  MIT License, see repository LICENSE file
 */
int main(void)
{
    Client *client = new Client;
    client->init();
    return client->loop();
}
